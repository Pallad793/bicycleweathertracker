package com.example.bicycleweathertracker;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Objects;

public class MarkerInfoView implements GoogleMap.InfoWindowAdapter {
    private LayoutInflater layoutInflater;

    private Location location;
    private Route route;
    private static Integer checkpointsCounter = 1;
    private FloatingActionButton addButton;
    private FloatingActionButton deleteButton;
    private FloatingActionButton saveRouteButtonLayout;

    private ArrayList<Marker> markers;

    MarkerInfoView(LayoutInflater layoutInflater, Route route,
                   FloatingActionButton addButton, FloatingActionButton deleteButton,
                   FloatingActionButton saveRouteButtonLayout, ArrayList<Marker> markers) {
        this.route = route;
        this.layoutInflater = layoutInflater;
        this.addButton = addButton;
        this.deleteButton = deleteButton;
        this.saveRouteButtonLayout = saveRouteButtonLayout;
        this.markers = markers;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        View markerItemView = layoutInflater.inflate(R.layout.marker_info_window, null);

        location = (Location) marker.getTag();
        if (location.getSequenceNumber() > 0) {
            TextView checkpointNumberTextView = markerItemView.findViewById(R.id.checkpointNumberTextView);
            checkpointNumberTextView.setVisibility(View.VISIBLE);
            checkpointNumberTextView.setText("Checkpoint no. " + location.getSequenceNumber().toString());
        }
        TextView latitudeTextView = markerItemView.findViewById(R.id.latitudeTextView);
        latitudeTextView.setText(location.getLatitude().toString());
        TextView longitudeTextView = markerItemView.findViewById(R.id.longitudeTextView);
        longitudeTextView.setText(location.getLongitude().toString());

        if (location.getSequenceNumber() == 0) {
            saveRouteButtonLayout.hide();
            addButton.show();
            addButton.setOnClickListener(click -> addCheckpointToRoute(marker));
            deleteButton.hide();
        } else {
            addButton.hide();
        }
        saveRouteButtonLayout.hide();
        deleteButton.show();
        deleteButton.setOnClickListener(click -> deleteCheckpoint());

        return markerItemView;
    }

    private void addCheckpointToRoute(Marker marker) {
        location.setSequenceNumber(checkpointsCounter);
        route.addCheckpoint(location);
        checkpointsCounter += 1;

        marker.showInfoWindow();
    }

    private void deleteCheckpoint() {
        deleteLocationFromRouteAndUpdateCheckpoints();
        deleteMarkerFromMap();
        addButton.hide();
        deleteButton.hide();
        saveRouteButtonLayout.show();
    }

    private void deleteLocationFromRouteAndUpdateCheckpoints() {
        if (route.containsLocation(location)) {
            checkpointsCounter -= 1;
            route.deleteCheckpoint(location);
            for (Location location : route.getCheckpoints()) {
                if (location.getSequenceNumber() > this.location.getSequenceNumber()) {
                    location.setSequenceNumber(location.getSequenceNumber()-1);
                }
            }
        }
    }

    private void deleteMarkerFromMap() {
        for (Marker marker : markers) {
            Location markerLocation = (Location) marker.getTag();
            if (Objects.equals(markerLocation, location)) {
                marker.setTag(null);
                marker.remove();
                markers.remove(marker);
                break;
            }
        }
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    public static void resetCheckpointsCounter(){
        checkpointsCounter = 1;
    }
}