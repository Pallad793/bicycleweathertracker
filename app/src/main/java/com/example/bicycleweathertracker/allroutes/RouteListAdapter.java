package com.example.bicycleweathertracker.allroutes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.bicycleweathertracker.Location;
import com.example.bicycleweathertracker.R;
import com.example.bicycleweathertracker.Route;
import com.example.bicycleweathertracker.RouteClickListener;
import com.example.bicycleweathertracker.database.converters.LocationHelper;
import com.example.bicycleweathertracker.database.converters.RouteHelper;
import com.example.bicycleweathertracker.database.repository.LocationRepository;
import com.example.bicycleweathertracker.database.repository.RouteRepository;
import com.example.bicycleweathertracker.database.repository.SystemConfigurationRepository;
import com.example.bicycleweathertracker.notifications.AlarmReceiver;
import com.example.bicycleweathertracker.notifications.NotificationScheduler;
import com.example.bicycleweathertracker.weatherapi.WeatherHelper;
import com.google.android.material.snackbar.Snackbar;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.example.bicycleweathertracker.utils.DateTimeUtil.formatDateTimeToView;
import static com.example.bicycleweathertracker.utils.ExpectedArrivalUtil.countExpectedArrivalDateTime;

public class RouteListAdapter extends ArrayAdapter<Route> implements View.OnClickListener {

    private static class ViewHolder {

        private TextView routeName;
        private TextView routeDistance;
        private TextView routeStartTimeLabel;
        private TextView routeStartTime;
        private TextView routeEndTimeLabel;
        private TextView routeEndTime;
        private ImageView actionImage;
    }
    public RouteListAdapter(@NonNull Context context, int resource, @NonNull List<Route> objects) {
        super(context, resource, objects);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.one_route_card_view, null);
            viewHolder.routeName = convertView.findViewById(R.id.routeName);
            viewHolder.routeDistance = convertView.findViewById(R.id.routeDistance);
            viewHolder.routeStartTimeLabel = convertView.findViewById(R.id.routeStartTimeLabel);
            viewHolder.routeStartTime = convertView.findViewById(R.id.routeStartTime);
            viewHolder.routeEndTimeLabel = convertView.findViewById(R.id.routeEndTimeLabel);
            viewHolder.routeEndTime = convertView.findViewById(R.id.routeEndTime);
            viewHolder.actionImage = convertView.findViewById(R.id.actionImage);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Route route = getItem(position);
        if (route != null) {
            viewHolder.routeName.setText(Objects.toString(route.getName(), ""));
            long distance = route.getDistance();
            String distanceText;
            if (distance < 1000) {
                distanceText = String.format("%s m", distance);
            } else {
                distanceText = String.format("%s km", distance / 1000);
            }
            viewHolder.routeDistance.setText(distanceText);
            viewHolder.actionImage.setOnClickListener(this);
            viewHolder.actionImage.setTag(position);

            if (route.isRunning()) {
                viewHolder.actionImage.setImageResource(R.drawable.ic_stop_black_24dp);
                viewHolder.routeStartTimeLabel.setVisibility(View.VISIBLE);
                viewHolder.routeStartTime.setVisibility(View.VISIBLE);
                viewHolder.routeStartTime.setText(formatDateTimeToView(route.getStartTime()));
                viewHolder.routeEndTimeLabel.setVisibility(View.VISIBLE);
                viewHolder.routeEndTime.setVisibility(View.VISIBLE);
                viewHolder.routeEndTime.setText(formatDateTimeToView(route.getExpectedArrival()));
            } else {
                viewHolder.actionImage.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                viewHolder.routeStartTimeLabel.setVisibility(View.GONE);
                viewHolder.routeStartTime.setVisibility(View.GONE);
                viewHolder.routeEndTimeLabel.setVisibility(View.GONE);
                viewHolder.routeEndTime.setVisibility(View.GONE);
            }

        }
        convertView.setOnClickListener(new RouteClickListener(getContext(), route));

        return convertView;
    }

    @Override
    public void onClick(View v) {
        int position = (Integer) v.getTag();
        Route route = getItem(position);
        if (route != null && route.isCanBeStarted()) {
            if (route.isRunning()) {
                route.setRunning(false);
                route.setStartTime(null);
                route.setExpectedArrival(null);
                for (int i = 0; i < getCount(); ++i) {
                    Optional.ofNullable(getItem(i)).ifPresent(item -> item.setCanBeStarted(true));
                }
                route.setAverageSpeed(null);
                NotificationScheduler.cancelReminder(getContext(), AlarmReceiver.class);
            } else {
                route.setRunning(true);
                DateTime startTime = new DateTime();
                route.setStartTime(startTime);
                fillExpectedArrivalDateTime(route, startTime);
                for (int i = 0; i < getCount(); ++i) {
                    if (position != i) {
                        Optional.ofNullable(getItem(i)).ifPresent(item -> item.setCanBeStarted(false));
                    }
                }
                route.setAverageSpeed(SystemConfigurationRepository.getAverageSpeedOrDefault(getContext()));
                scheduleFirstNotification(route);
            }
            RouteRepository.update(getContext(), RouteHelper.createRouteEntity(route));
            LocationRepository.updateAll(getContext(), LocationHelper.createLocationEntities(route));
            this.notifyDataSetChanged();
        } else if (route != null) {
            Snackbar.make(v, "Another route is already running", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void fillExpectedArrivalDateTime(Route route, DateTime startTime) {
        DateTime previousExpectedArrivalTime = startTime;

        double averageSpeed = SystemConfigurationRepository.getAverageSpeedOrDefault(getContext());

        for (Location location : route.getCheckpoints()) {
            DateTime expectedArrival = countExpectedArrivalDateTime(averageSpeed, previousExpectedArrivalTime, location.getDistanceInMeters());
            location.setExpectedArrival(expectedArrival);
            previousExpectedArrivalTime = new DateTime(expectedArrival);
        }
        route.setExpectedArrival(previousExpectedArrivalTime);
        WeatherHelper.fillLocationsWeather(route.getCheckpoints());
    }

    private void scheduleFirstNotification(Route route) {
        List<Location> checkpoints = route.getCheckpoints();
        if (checkpoints.size() > 1) {
            NotificationScheduler.scheduleNotification(getContext(), route.getName(), checkpoints.get(0), checkpoints.get(1));
        }
    }

}
