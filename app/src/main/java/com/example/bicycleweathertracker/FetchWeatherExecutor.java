package com.example.bicycleweathertracker;

import android.os.AsyncTask;

import com.example.bicycleweathertracker.weatherapi.WeatherUtil;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.HourlyDataPoint;

public class FetchWeatherExecutor extends AsyncTask<Location, Integer, Forecast> {

    public FetchWeatherExecutor() {
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Forecast doInBackground(Location[] checkpoints) {
        try {
            List<Location> locationList = Arrays.asList(checkpoints);
            if (locationList.isEmpty()) {
                return null;
            }

            for (Location location : locationList) {
                Forecast forecast;
                DateTime expectedArrival = location.getExpectedArrival();
                if (expectedArrival.getHourOfDay() == 23) {
                    forecast = WeatherUtil.getWeather(location.getLongitude(), location.getLatitude(), location.getExpectedArrival().plusHours(1));
                } else {
                    forecast = WeatherUtil.getWeather(location.getLongitude(), location.getLatitude(), location.getExpectedArrival());
                }
                if (forecast == null) {
                    continue;
                }
                if (expectedArrival.getHourOfDay() == 23) {
                    location.setHourlyDataPoint(forecast.getHourly().getData().get(0));
                } else {
                    for (HourlyDataPoint hourlyDataPoint : forecast.getHourly().getData()) {
                        DateTime dateTime = new DateTime(hourlyDataPoint.getTime().getEpochSecond()*1000);
                        dateTime.plusHours(1);
                        if (dateTime.getDayOfYear() == expectedArrival.getDayOfYear()
                                && dateTime.getHourOfDay() > expectedArrival.getHourOfDay()) {
                            location.setHourlyDataPoint(hourlyDataPoint);
                            break;
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Forecast forecast) {
    }
}
