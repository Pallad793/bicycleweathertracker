package com.example.bicycleweathertracker.weatherapi;

import com.example.bicycleweathertracker.FetchWeatherExecutor;
import com.example.bicycleweathertracker.Location;

import java.util.List;
import java.util.concurrent.ExecutionException;

import tk.plogitech.darksky.forecast.model.Forecast;

public class WeatherHelper {

    public static void fillLocationsWeather(List<Location> locations) {
        FetchWeatherExecutor fetchWeatherExecutor = new FetchWeatherExecutor();
        try {
            fetchWeatherExecutor.execute(locations.toArray(new Location[0]));
            Forecast forecast = fetchWeatherExecutor.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
