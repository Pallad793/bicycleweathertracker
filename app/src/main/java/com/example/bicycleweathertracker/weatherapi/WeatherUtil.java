package com.example.bicycleweathertracker.weatherapi;

//import org.joda.time.DateTime;

import android.content.Context;
import android.graphics.Color;

import com.thbs.skycons.library.CloudFogView;
import com.thbs.skycons.library.CloudHvRainView;
import com.thbs.skycons.library.CloudMoonView;
import com.thbs.skycons.library.CloudRainView;
import com.thbs.skycons.library.CloudSnowView;
import com.thbs.skycons.library.CloudSunView;
import com.thbs.skycons.library.CloudThunderView;
import com.thbs.skycons.library.CloudView;
import com.thbs.skycons.library.MoonView;
import com.thbs.skycons.library.SkyconView;
import com.thbs.skycons.library.SunView;
import com.thbs.skycons.library.WindView;

import org.joda.time.DateTime;

import java.time.Instant;

import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.APIKey;
import tk.plogitech.darksky.forecast.ForecastException;
import tk.plogitech.darksky.forecast.ForecastRequest;
import tk.plogitech.darksky.forecast.ForecastRequestBuilder;
import tk.plogitech.darksky.forecast.GeoCoordinates;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

public class WeatherUtil {
    private static final DarkSkyJacksonClient darkSkyJacksonClient = new DarkSkyJacksonClient();
    private static final String WEATHER_API_KEY = "5720cfc6642a014c987a04721975e659";
    private static final String OVERRIDED_URL = "https://api.darksky.net/forecast/##key##/##latitude##,##longitude##,";

    public static Forecast getWeather(Double longitude, Double latitude, DateTime dateTime) {
        ForecastRequest request = new ForecastRequestBuilder()
                .key(new APIKey(WEATHER_API_KEY))
                .location(new GeoCoordinates(new Longitude(longitude), new Latitude(latitude)))
                .time(Instant.ofEpochMilli(dateTime.getMillis()))
                .exclude(ForecastRequestBuilder.Block.alerts)
                .exclude(ForecastRequestBuilder.Block.flags)
                .exclude(ForecastRequestBuilder.Block.daily)
                .language(ForecastRequestBuilder.Language.en)
                .build();
        try {
            return darkSkyJacksonClient.forecast(request);
        } catch (ForecastException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static SkyconView getWeatherView(String iconName, Context context) {
        switch (iconName) {
            case "clear-day":
                return new SunView(context, false, true, Color.BLUE, Color.WHITE);
            case "clear-night":
                return new MoonView(context, false, true, Color.BLUE, Color.WHITE);
            case "rain":
                return new CloudRainView(context, false, true, Color.BLUE, Color.WHITE);
            case "snow":
            case "sleet":
                return new CloudSnowView(context, false, true, Color.BLUE, Color.WHITE);
            case "wind":
                return new WindView(context, false, true, Color.BLUE, Color.WHITE);
            case "fog":
                return new CloudFogView(context, false, true, Color.BLUE, Color.WHITE);
            case "cloudy":
                return new CloudView(context, false, true, Color.BLUE, Color.WHITE);
            case "partly-cloudy-day":
                return new CloudSunView(context, false, true, Color.BLUE, Color.WHITE);
            case "partly-cloudy-night":
                return new CloudMoonView(context, false, true, Color.BLUE, Color.WHITE);
            case "thunderstorm":
                return new CloudThunderView(context, false, true, Color.BLUE, Color.WHITE);
            case "hail":
                return new CloudHvRainView(context, false, true, Color.BLUE, Color.WHITE);
            default:
                if (!iconName.isEmpty()) {
                    System.out.println("!!!!!! Not defined IconName: " + iconName);
                }
                return new SkyconView(context);

        }
    }
}
