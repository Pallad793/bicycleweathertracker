package com.example.bicycleweathertracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllRoutes {
    private final Map<String, Route> routes;

    public AllRoutes() {
        this.routes = new HashMap<>();
    }

    public AllRoutes(List<Route> routes) {
        this.routes = new HashMap<>();
        routes.forEach(route -> this.routes.put(route.getName(), route));

    }

    public Route getRoute(String routeName) {
        return routes.get(routeName);
    }

    public void addRoute(Route route) {
        routes.put(route.getName(), route);
    }

    public List<Route> getAllRoutesList() {
        return new ArrayList<>(routes.values());
    }
}