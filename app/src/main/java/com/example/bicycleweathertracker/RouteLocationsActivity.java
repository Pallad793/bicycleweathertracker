package com.example.bicycleweathertracker;

import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bicycleweathertracker.routelocations.RouteLocationListAdapter;

public class RouteLocationsActivity extends NavigationActivity {

    private Route route;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewStub stub = findViewById(R.id.layout_stub);
        stub.setLayoutResource(R.layout.activity_route_locations);
        this.route = (Route) getIntent().getSerializableExtra("route");
        stub.inflate();

        ListView allRouteLocationsListView = findViewById(R.id.allRouteLocationsListView);
        TextView noRouteLocationsTextView = findViewById(R.id.noRouteLocationsTextView);

        if (!route.getCheckpoints().isEmpty()) {
            noRouteLocationsTextView.setVisibility(View.GONE);
        }

        RouteLocationListAdapter routeLocationListAdapter = new RouteLocationListAdapter(this, R.layout.one_route_card_view, route.getCheckpoints(), route.isRunning());
        allRouteLocationsListView.setAdapter(routeLocationListAdapter);
    }
}
