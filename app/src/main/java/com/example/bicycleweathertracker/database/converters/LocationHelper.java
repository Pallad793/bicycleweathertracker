package com.example.bicycleweathertracker.database.converters;

import androidx.annotation.Nullable;

import com.example.bicycleweathertracker.Location;
import com.example.bicycleweathertracker.Route;
import com.example.bicycleweathertracker.database.entity.LocationEntity;

import java.util.List;
import java.util.stream.Collectors;

public class LocationHelper {

    public static List<LocationEntity> createLocationEntities(Route route) {
        return route.getCheckpoints().stream()
                .map(LocationHelper::createLocationEntity)
                .collect(Collectors.toList());
    }

    public static LocationEntity createLocationEntity(Location createdLocation) {
        return new LocationEntity.Builder()
                .withSequenceNumber(createdLocation.getSequenceNumber())
                .withLongitude(createdLocation.getLongitude())
                .withLatitude(createdLocation.getLatitude())
                .withExpectedArrivalTime(createdLocation.getExpectedArrival())
                .withRouteName(createdLocation.getRouteName())
                .withDistanceInMeters(createdLocation.getDistanceInMeters())
                .build();
    }

    @Nullable
    public static Location createLocation(LocationEntity locationEntity) {
        if (locationEntity == null) {
            return null;
        }

        return new Location.Builder()
                .withSequenceNumber(locationEntity.getSequenceNumber())
                .withLongitude(locationEntity.getLongitude())
                .withLatitude(locationEntity.getLatitude())
                .withExpectedArrivalTime(locationEntity.getExpectedArrival())
                .withRouteName(locationEntity.getRouteName())
                .withDistanceInMeters(locationEntity.getDistanceInMeters())
                .build();
    }
}
