package com.example.bicycleweathertracker.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.bicycleweathertracker.database.converters.TimestampConverter;

import org.joda.time.DateTime;

import java.io.Serializable;

@Entity(indices = {@Index(value = "name")})
public class RouteEntity implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "name")
    @NonNull
    private String name;

    @ColumnInfo(name = "is_running")
    private boolean isRunning;

    @ColumnInfo(name = "start_time")
    @TypeConverters({TimestampConverter.class})
    private DateTime startTime;

    @ColumnInfo(name = "expected_arrival_time")
    @TypeConverters({TimestampConverter.class})
    private DateTime expectedArrivalTime;

    @ColumnInfo(name = "average_speed")
    private Double averageSpeed;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(DateTime expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public static class Builder {

        private RouteEntity routeEntity;

        public Builder() {
            this.routeEntity = new RouteEntity();
        }

        public Builder withName(String name) {
            this.routeEntity.setName(name);
            return this;
        }

        public Builder withIsRunning(boolean isRunning) {
            this.routeEntity.setRunning(isRunning);
            return this;
        }

        public Builder withStartTime(DateTime startTime) {
            this.routeEntity.setStartTime(startTime);
            return this;
        }

        public Builder withExpectedArrivalTime(DateTime expectedArrivalTime) {
            this.routeEntity.setExpectedArrivalTime(expectedArrivalTime);
            return this;
        }

        public Builder withAverageSpeed(Double averageSpeed) {
            this.routeEntity.setAverageSpeed(averageSpeed);
            return this;
        }

        public RouteEntity build() {
            return routeEntity;
        }
    }
}
