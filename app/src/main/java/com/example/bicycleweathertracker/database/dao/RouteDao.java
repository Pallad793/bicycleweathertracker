package com.example.bicycleweathertracker.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.bicycleweathertracker.database.entity.RouteEntity;

import java.util.List;

@Dao
public interface RouteDao {

    @Query("SELECT * FROM RouteEntity")
    List<RouteEntity> getAllRoutes();

    @Query("SELECT * FROM RouteEntity re WHERE re.name=:name")
    RouteEntity getRouteByName(String name);

    @Query("SELECT * FROM RouteEntity re WHERE re.is_running=1 LIMIT 1")
    RouteEntity getRunningRoute();

    @Insert
    void insert(RouteEntity routeEntity);

    @Delete
    void delete(RouteEntity routeEntity);

    @Update
    void update(RouteEntity routeEntity);

    @Query("DELETE FROM RouteEntity")
    void deleteAll();
}
