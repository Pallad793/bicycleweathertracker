package com.example.bicycleweathertracker.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.bicycleweathertracker.database.entity.LocationEntity;

import java.util.List;

@Dao
public interface LocationDao {

    @Query("SELECT * FROM LocationEntity where route_name=:routeName")
    List<LocationEntity> getAllLocationsForRoute(String routeName);

    @Query("SELECT * FROM LocationEntity where route_name=:routeName and sequence_number=:sequenceNumber LIMIT 1")
    LocationEntity getLocationForRouteNameWithSequenceNumber(String routeName, int sequenceNumber);

    @Insert
    void insert(LocationEntity locationEntity);

    @Delete
    void delete(LocationEntity locationEntity);

    @Update
    void update(LocationEntity locationEntity);

    @Query("DELETE FROM LocationEntity")
    void deleteAll();
}
