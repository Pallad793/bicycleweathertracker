package com.example.bicycleweathertracker.database.repository;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import com.example.bicycleweathertracker.Location;
import com.example.bicycleweathertracker.database.DatabaseClient;
import com.example.bicycleweathertracker.database.converters.LocationHelper;
import com.example.bicycleweathertracker.database.dao.LocationDao;
import com.example.bicycleweathertracker.database.entity.LocationEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class LocationRepository {

    public static List<Location> getAllLocationsForRoute(Context context, String routeName) {

        class SelectLocations extends AsyncTask<Void, Void, List<LocationEntity>> {

            @Override
            protected List<LocationEntity> doInBackground(Void... voids) {
                return getLocationDao(context).getAllLocationsForRoute(routeName);
            }
        }

        try {
            List<LocationEntity> allLocations = new SelectLocations().execute().get();
            return allLocations.stream()
                    .map(locationEntity -> LocationHelper.createLocation(locationEntity))
                    .collect(Collectors.toList());
        } catch (ExecutionException | InterruptedException e) {
            return new ArrayList<>();
        }
    }

    public static Location getLocationForRouteNameWithSequenceNumber(Context context, String routeName, int sequenceNumber) {

        class SelectLocations extends AsyncTask<Void, Void, LocationEntity> {

            @Override
            protected LocationEntity doInBackground(Void... voids) {
                return getLocationDao(context).getLocationForRouteNameWithSequenceNumber(routeName, sequenceNumber);
            }
        }

        try {
            LocationEntity locationEntity = new SelectLocations().execute().get();
            return LocationHelper.createLocation(locationEntity);
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }


    public static Integer insert(Context context, LocationEntity locationEntity) {
        class InsertRoute extends AsyncTask<Void, Void, Integer> {

            @Override
            protected Integer doInBackground(Void... voids) {
                try {
                    getLocationDao(context).insert(locationEntity);
                    return 1;
                } catch (SQLiteConstraintException e) {
                    return -1;
                } catch (Exception e) {
                    return 0;
                }
            }
        }

        try {
            return new InsertRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
            return 0;
        }
    }

    public static Integer insertAll(Context context, List<LocationEntity> locationEntities) {
        class InsertRoute extends AsyncTask<Void, Void, Integer> {

            @Override
            protected Integer doInBackground(Void... voids) {
                try {
                    locationEntities.forEach(locationEntity ->
                            getLocationDao(context).insert(locationEntity));
                    return 1;
                } catch (SQLiteConstraintException e) {
                    return -1;
                } catch (Exception e) {
                    return 0;
                }
            }
        }

        try {
            return new InsertRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
            return 0;
        }
    }

    public static void delete(Context context, LocationEntity locationEntity) {
        class DeleteRoute extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getLocationDao(context).delete(locationEntity);
                return null;
            }
        }

        try {
            new DeleteRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void update(Context context, LocationEntity locationEntity) {
        class UpdateRoute extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getLocationDao(context).update(locationEntity);
                return null;
            }
        }

        try {
            new UpdateRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void updateAll(Context context, List<LocationEntity> locationEntities) {
        class UpdateRoutes extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                locationEntities.forEach(locationEntity ->
                        getLocationDao(context).update(locationEntity));
                return null;
            }
        }

        try {
            new UpdateRoutes().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void deleteAll(Context context) {
        class DeleteAllRoutes extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getLocationDao(context).deleteAll();
                return null;
            }
        }

        try {
            new DeleteAllRoutes().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }

    }

    private static LocationDao getLocationDao(Context context) {
        return DatabaseClient.getInstance(context)
                .getAppDatabase()
                .locationDao();
    }
}
