package com.example.bicycleweathertracker.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.TypeConverters;

import com.example.bicycleweathertracker.database.converters.TimestampConverter;

import org.joda.time.DateTime;

import java.io.Serializable;

import static androidx.room.ForeignKey.CASCADE;

@Entity(indices = {@Index(value = {"sequence_number", "route_name"}), @Index(value = "route_name")},
        foreignKeys = @ForeignKey(entity = RouteEntity.class,
                parentColumns = "name",
                childColumns = "route_name",
                onDelete = CASCADE),
        primaryKeys = {"sequence_number", "route_name"})
public class LocationEntity implements Serializable {

    @ColumnInfo(name = "sequence_number")
    private int sequenceNumber;

    @ColumnInfo(name = "latitude")
    private Double latitude;

    @ColumnInfo(name = "longitude")
    private Double longitude;

    @ColumnInfo(name = "expected_arrival")
    @TypeConverters({TimestampConverter.class})
    private DateTime expectedArrival;

    @ColumnInfo(name = "route_name")
    @NonNull
    private String routeName;

    @ColumnInfo(name = "distance_meters")
    private long distanceInMeters;

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public DateTime getExpectedArrival() {
        return expectedArrival;
    }

    public void setExpectedArrival(DateTime expectedArrival) {
        this.expectedArrival = expectedArrival;
    }

    public long getDistanceInMeters() {
        return distanceInMeters;
    }

    public void setDistanceInMeters(long distanceInMeters) {
        this.distanceInMeters = distanceInMeters;
    }

    public static class Builder {

        private LocationEntity locationEntity;

        public Builder() {
            this.locationEntity = new LocationEntity();
        }

        public Builder withSequenceNumber(int number) {
            this.locationEntity.setSequenceNumber(number);
            return this;
        }

        public Builder withLongitude(Double longitude) {
            this.locationEntity.setLongitude(longitude);
            return this;
        }

        public Builder withLatitude(Double latitude) {
            this.locationEntity.setLatitude(latitude);
            return this;
        }

        public Builder withExpectedArrivalTime(DateTime expectedArrivalTime) {
            this.locationEntity.setExpectedArrival(expectedArrivalTime);
            return this;
        }

        public Builder withRouteName(String routeName) {
            this.locationEntity.setRouteName(routeName);
            return this;
        }

        public Builder withDistanceInMeters(long distanceInMeters) {
            this.locationEntity.setDistanceInMeters(distanceInMeters);
            return this;
        }

        public LocationEntity build() {
            return locationEntity;
        }
    }
}
