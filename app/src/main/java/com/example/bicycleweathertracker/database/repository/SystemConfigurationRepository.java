package com.example.bicycleweathertracker.database.repository;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import com.example.bicycleweathertracker.config.Constants;
import com.example.bicycleweathertracker.database.DatabaseClient;
import com.example.bicycleweathertracker.database.dao.SystemConfigurationDao;
import com.example.bicycleweathertracker.database.entity.SystemConfiguration;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class SystemConfigurationRepository {

    public static double getAverageSpeedOrDefault(Context context) {
        return Optional.ofNullable(SystemConfigurationRepository.getSystemConfigurationByKey(context, Constants.AVERAGE_SPEED_KEY))
                .map(Double::parseDouble)
                .orElse(Constants.DEFAULT_AVERAGE_SPEED);
    }

    public static String getSystemConfigurationByKey(Context context, String key) {
        class SelectConfiguration extends AsyncTask<Void, Void, String> {

            @Override
            protected String doInBackground(Void... voids) {
                return Optional.ofNullable(getSystemConfigurationDao(context).getConfigurationByKey(key))
                        .map(SystemConfiguration::getValue)
                        .orElse(null);
            }
        }

        try {
            return new SelectConfiguration().execute().get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }


    public static Integer insert(Context context, SystemConfiguration systemConfiguration) {
        class InsertSystemConfiguration extends AsyncTask<Void, Void, Integer> {

            @Override
            protected Integer doInBackground(Void... voids) {
                try {
                    getSystemConfigurationDao(context).insert(systemConfiguration);
                    return 1;
                } catch (SQLiteConstraintException e) {
                    return -1;
                } catch (Exception e) {
                    return 0;
                }
            }
        }

        try {
            return new InsertSystemConfiguration().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
            return 0;
        }
    }

    public static void delete(Context context, SystemConfiguration systemConfiguration) {
        class DeleteSystemConfiguration extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getSystemConfigurationDao(context).delete(systemConfiguration);
                return null;
            }
        }

        try {
            new DeleteSystemConfiguration().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void update(Context context, SystemConfiguration systemConfiguration) {
        class UpdateSystemConfiguration extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getSystemConfigurationDao(context).update(systemConfiguration);
                return null;
            }
        }

        try {
            new UpdateSystemConfiguration().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void deleteAll(Context context) {
        class DeleteAllSystemConfigurations extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getSystemConfigurationDao(context).deleteAll();
                return null;
            }
        }

        try {
            new DeleteAllSystemConfigurations().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }

    }

    private static SystemConfigurationDao getSystemConfigurationDao(Context context) {
        return DatabaseClient.getInstance(context)
                .getAppDatabase()
                .systemConfigurationDao();
    }
}
