package com.example.bicycleweathertracker.database.converters;

import androidx.room.TypeConverter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Optional;

public class TimestampConverter {
    private static final String TIME_STAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @TypeConverter
    public static DateTime fromTimestamp(String value) {
        if (value != null) {
            return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(value);
        }
        return null;
    }

    @TypeConverter
    public static String fromDate(DateTime date) {
        if (date == null) {
            return null;
        }
        return Optional.ofNullable(date)
                .map(dateTime -> dateTime.toString(TIME_STAMP_FORMAT))
                .orElse(null);
    }
}
