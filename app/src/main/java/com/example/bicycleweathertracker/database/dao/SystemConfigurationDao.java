package com.example.bicycleweathertracker.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.bicycleweathertracker.database.entity.SystemConfiguration;

@Dao
public interface SystemConfigurationDao {

    @Query("SELECT * FROM SystemConfiguration WHERE `key`=:key LIMIT 1")
    SystemConfiguration getConfigurationByKey(String key);

    @Insert
    void insert(SystemConfiguration systemConfiguration);

    @Delete
    void delete(SystemConfiguration systemConfiguration);

    @Update
    void update(SystemConfiguration systemConfiguration);

    @Query("DELETE FROM SystemConfiguration")
    void deleteAll();
}
