package com.example.bicycleweathertracker.database.converters;

import android.content.Context;

import com.example.bicycleweathertracker.R;
import com.example.bicycleweathertracker.Route;
import com.example.bicycleweathertracker.database.entity.RouteEntity;

public class RouteHelper {

    public static RouteEntity createRouteEntity(Route createdRoute) {
        return new RouteEntity.Builder()
                .withName(createdRoute.getName())
                .withIsRunning(createdRoute.isRunning())
                .withStartTime(createdRoute.getStartTime())
                .withExpectedArrivalTime(createdRoute.getExpectedArrival())
                .withAverageSpeed(createdRoute.getAverageSpeed())
                .build();
    }

    public static Route createRoute(Context context, RouteEntity routeEntity) {
        if (routeEntity == null) {
            return null;
        }
        return new Route.Builder()
                .withApiKey(context.getString(R.string.google_maps_key))
                .withName(routeEntity.getName())
                .withIsRunning(routeEntity.isRunning())
                .withStartTime(routeEntity.getStartTime())
                .withExpectedArrivalTime(routeEntity.getExpectedArrivalTime())
                .withAverageSpeed(routeEntity.getAverageSpeed())
                .build();
    }
}
