package com.example.bicycleweathertracker.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.bicycleweathertracker.database.dao.LocationDao;
import com.example.bicycleweathertracker.database.dao.RouteDao;
import com.example.bicycleweathertracker.database.dao.SystemConfigurationDao;
import com.example.bicycleweathertracker.database.entity.LocationEntity;
import com.example.bicycleweathertracker.database.entity.RouteEntity;
import com.example.bicycleweathertracker.database.entity.SystemConfiguration;

@Database(entities = {RouteEntity.class, LocationEntity.class, SystemConfiguration.class}, version = 7, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract RouteDao routeDao();

    public abstract LocationDao locationDao();

    public abstract SystemConfigurationDao systemConfigurationDao();
}
