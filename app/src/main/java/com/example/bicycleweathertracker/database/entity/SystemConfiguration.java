package com.example.bicycleweathertracker.database.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(indices = {@Index(value = "key")})
public class SystemConfiguration implements Serializable {

    @PrimaryKey
    @ColumnInfo(name = "key")
    @NonNull
    private String key;

    @ColumnInfo(name = "value")
    private String value;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static class Builder {

        private SystemConfiguration systemConfiguration;

        public Builder() {
            this.systemConfiguration = new SystemConfiguration();
        }

        public Builder withKey(String key) {
            this.systemConfiguration.setKey(key);
            return this;
        }

        public Builder withValue(String value) {
            this.systemConfiguration.setValue(value);
            return this;
        }

        public SystemConfiguration build() {
            return systemConfiguration;
        }
    }
}
