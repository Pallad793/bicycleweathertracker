package com.example.bicycleweathertracker.database.repository;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;

import com.example.bicycleweathertracker.Route;
import com.example.bicycleweathertracker.database.DatabaseClient;
import com.example.bicycleweathertracker.database.converters.RouteHelper;
import com.example.bicycleweathertracker.database.dao.RouteDao;
import com.example.bicycleweathertracker.database.entity.RouteEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class RouteRepository {

    public static List<Route> getAllRoutes(Context context) {

        class SelectRoutes extends AsyncTask<Void, Void, List<RouteEntity>> {

            @Override
            protected List<RouteEntity> doInBackground(Void... voids) {
                return getRouteDao(context).getAllRoutes();
            }
        }

        try {
            List<RouteEntity> allRoutes = new SelectRoutes().execute().get();
            return allRoutes.stream()
                    .map(routeEntity -> RouteHelper.createRoute(context, routeEntity))
                    .collect(Collectors.toList());
        } catch (ExecutionException | InterruptedException e) {
            return new ArrayList<>();
        }
    }

    public static RouteEntity getRouteByName(Context context, String name) {
        class SelectRoute extends AsyncTask<Void, Void, RouteEntity> {

            @Override
            protected RouteEntity doInBackground(Void... voids) {
                return getRouteDao(context).getRouteByName(name);
            }
        }

        try {
            return new SelectRoute().execute().get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }

    public static boolean isAnyRouteRunning(Context context) {
        class SelectRoute extends AsyncTask<Void, Void, RouteEntity> {

            @Override
            protected RouteEntity doInBackground(Void... voids) {
                return getRouteDao(context).getRunningRoute();
            }
        }

        try {
            RouteEntity routeEntity = new SelectRoute().execute().get();
            return routeEntity != null;
        } catch (ExecutionException | InterruptedException e) {
            return false;
        }
    }

    public static RouteEntity getRunningRoute(Context context) {
        class SelectRoute extends AsyncTask<Void, Void, RouteEntity> {

            @Override
            protected RouteEntity doInBackground(Void... voids) {
                return getRouteDao(context).getRunningRoute();
            }
        }

        try {
            return new SelectRoute().execute().get();
        } catch (ExecutionException | InterruptedException e) {
            return null;
        }
    }


    public static Integer insert(Context context, RouteEntity routeEntity) {
        class InsertRoute extends AsyncTask<Void, Void, Integer> {

            @Override
            protected Integer doInBackground(Void... voids) {
                try {
                    getRouteDao(context).insert(routeEntity);
                    return 1;
                } catch (SQLiteConstraintException e) {
                    return -1;
                } catch (Exception e) {
                    return 0;
                }
            }
        }

        try {
            return new InsertRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
            return 0;
        }
    }

    public static void delete(Context context, RouteEntity routeEntity) {
        class DeleteRoute extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getRouteDao(context).delete(routeEntity);
                return null;
            }
        }

        try {
            new DeleteRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void update(Context context, RouteEntity routeEntity) {
        class UpdateRoute extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getRouteDao(context).update(routeEntity);
                return null;
            }
        }

        try {
            new UpdateRoute().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }
    }

    public static void deleteAll(Context context) {
        class DeleteAllRoutes extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getRouteDao(context).deleteAll();
                return null;
            }
        }

        try {
            new DeleteAllRoutes().execute().get();
        } catch (ExecutionException | InterruptedException ignored) {
        }

    }

    private static RouteDao getRouteDao(Context context) {
        return DatabaseClient.getInstance(context)
                .getAppDatabase()
                .routeDao();
    }
}
