package com.example.bicycleweathertracker.config;

public interface Constants {

    double DEFAULT_AVERAGE_SPEED = 15.0;
    String AVERAGE_SPEED_KEY = "average.speed.key";
}
