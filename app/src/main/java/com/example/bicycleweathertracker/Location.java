package com.example.bicycleweathertracker;


import com.example.bicycleweathertracker.directionsapi.ApiResult;
import com.google.android.gms.maps.model.LatLng;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import tk.plogitech.darksky.forecast.model.HourlyDataPoint;

public class Location implements Serializable {
    private Integer sequenceNumber = 0;
    private String routeName;
    private Double latitude;
    private Double longitude;
    private DateTime expectedArrival;
    private HourlyDataPoint hourlyDataPoint;
    private long distanceInMeters;
    private transient LatLng latLng;
    private transient ApiResult apiResult;

    private Location() {}

    Location(LatLng latLng, String routeName) {
        this.latLng = latLng;
        this.latitude = latLng.latitude;
        this.longitude = latLng.longitude;
        this.routeName = routeName;
        this.apiResult = new ApiResult();
    }

    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public String getRouteName() {
        return routeName;
    }

    private void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    private void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    private void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public void setApiResult(ApiResult apiResult) {
        this.apiResult = apiResult;
    }

    public List<LatLng> getPointsFromPreviousLocation() {
        return apiResult.getPoints();
    }

    public void setPointsFromPreviousLocation(List<LatLng> pointsFromPreviousLocation) {
        this.apiResult.setPoints(pointsFromPreviousLocation);
    }
    // TODO zmienić dystans gdy usuwamy punkt z początku lub końca
    public long getDistanceInMeters() {
        return Optional.ofNullable(apiResult)
                .map(ApiResult::getDistance)
                .filter(distance -> distance.inMeters != 0)
                .map(distance -> distance.inMeters)
                .orElse(distanceInMeters);
    }

    public DateTime getExpectedArrival() {
        return expectedArrival;
    }

    public void setExpectedArrival(DateTime expectedArrival) {
        this.expectedArrival = expectedArrival;
    }

    public HourlyDataPoint getHourlyDataPoint() {
        return hourlyDataPoint;
    }

    public void setHourlyDataPoint(HourlyDataPoint hourlyDataPoint) {
        this.hourlyDataPoint = hourlyDataPoint;
    }

    private void setDistanceInMeters(long distanceInMeters) {
        this.distanceInMeters = distanceInMeters;
    }

    public static class Builder {

        private Location location;

        public Builder() {
            this.location = new Location();
        }

        public Builder withSequenceNumber(int number) {
            this.location.setSequenceNumber(number);
            return this;
        }

        public Builder withLongitude(Double longitude) {
            this.location.setLongitude(longitude);
            return this;
        }

        public Builder withLatitude(Double latitude) {
            this.location.setLatitude(latitude);
            return this;
        }

        public Builder withExpectedArrivalTime(DateTime expectedArrivalTime) {
            this.location.setExpectedArrival(expectedArrivalTime);
            return this;
        }

        public Builder withRouteName(String routeName) {
            this.location.setRouteName(routeName);
            return this;
        }

        public Builder withDistanceInMeters(long distanceInMeters) {
            this.location.setDistanceInMeters(distanceInMeters);
            return this;
        }

        public Location build() {
            return location;
        }
    }
}