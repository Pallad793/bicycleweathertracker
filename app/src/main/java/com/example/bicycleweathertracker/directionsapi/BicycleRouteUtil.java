package com.example.bicycleweathertracker.directionsapi;

import com.example.bicycleweathertracker.Location;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.Distance;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class BicycleRouteUtil implements Serializable {

    private final String apiKey;

    public BicycleRouteUtil(String apiKey) {
        this.apiKey = apiKey;
    }

    public ApiResult getPathForLocations(List<Location> locations) throws Exception {
        LatLng origin = getLatLngForLocation(locations.get(0));
        LatLng destination = getLatLngForLocation(locations.get(locations.size() - 1));

        List<LatLng> wayPoints = getWayPointsFromLocationList(locations);
        DirectionsResult directionsResult = getDirectionsResultForWholeRoute(origin, destination, wayPoints);
        Distance distance = directionsResult.routes[0].legs[0].distance;
        List<com.google.android.gms.maps.model.LatLng> points = PolyUtil.decode(directionsResult.routes[0].overviewPolyline.getEncodedPath());
        return new ApiResult(points, distance);
    }

    private List<LatLng> getWayPointsFromLocationList(List<Location> locations) {
        List<LatLng> wayPoints = new ArrayList<>();

        for (Location location : locations) {
            wayPoints.add(getLatLngForLocation(location));
        }
        wayPoints.remove(0);
        wayPoints.remove(wayPoints.size() - 1);
        return wayPoints;
    }

    private LatLng getLatLngForLocation(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    private DirectionsResult getDirectionsResultForWholeRoute(LatLng origin, LatLng destination, List<LatLng> points) throws Exception {
        DateTime now = new DateTime();
        return DirectionsApi.newRequest(getGeoContext())
                .mode(TravelMode.BICYCLING)
                .origin(origin)
                .waypoints(points.toArray(new LatLng[0]))
                .destination(destination)
                .departureTime(now)
                .units(Unit.METRIC)
                .await();
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(apiKey)
                .setConnectTimeout(10, TimeUnit.SECONDS)
                .setReadTimeout(10, TimeUnit.SECONDS)
                .setWriteTimeout(10, TimeUnit.SECONDS);
    }
}
