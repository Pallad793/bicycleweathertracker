package com.example.bicycleweathertracker.directionsapi;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.model.Distance;

import java.util.ArrayList;
import java.util.List;

public class ApiResult {

    List<LatLng> points;

    Distance distance;

    public ApiResult() {
        this.points = new ArrayList<>();
        this.distance = new Distance();
    }

    public ApiResult(List<LatLng> points, Distance distance) {
        this.points = points;
        this.distance = distance;
    }

    public List<LatLng> getPoints() {
        return points;
    }

    public void setPoints(List<LatLng> points) {
        this.points = points;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Distance getDistance() {
        return distance;
    }
}
