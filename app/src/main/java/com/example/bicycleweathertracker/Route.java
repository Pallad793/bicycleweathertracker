package com.example.bicycleweathertracker;

import com.example.bicycleweathertracker.directionsapi.ApiResult;
import com.example.bicycleweathertracker.directionsapi.BicycleRouteUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Route implements Serializable {
    private boolean isRunning;
    private boolean canBeStarted = true;
    private DateTime startTime;
    private DateTime expectedArrival;
    private String name;
    private Double averageSpeed;
    private List<Location> checkpoints = new ArrayList<>();
    private transient Polyline polyline;
    private transient BicycleRouteUtil bicycleRouteUtil;

    private Route() {}

    public Route(String name, Polyline polyline, String apiKey) {
        this.name = name;
        this.polyline = polyline;
        this.bicycleRouteUtil = new BicycleRouteUtil(apiKey);
    }

    public void addCheckpoint(Location location) {
        checkpoints.add(location);
        if (checkpoints.size() > 1) {
            Location previousLocation = checkpoints.get(checkpoints.size() - 2);

            ApiResult apiResult = getApiResultForPoints(previousLocation, location);

            location.setApiResult(apiResult);
            List<LatLng> points = apiResult.getPoints();
            location.setPointsFromPreviousLocation(points);
            List<LatLng> newPolyline = polyline.getPoints();
            newPolyline.addAll(points);
            polyline.setPoints(newPolyline);
        }

    }

    public boolean containsLocation(Location location) {
        return checkpoints.contains(location);
    }

    public void deleteCheckpoint(Location location) {

        if (checkpoints.size() == 1) {
            checkpoints.remove(location);
            polyline.setPoints(new ArrayList<>());
            return;
        }

        int currentLocationIndex = checkpoints.indexOf(location);
        int lastLocationIndex = checkpoints.size() - 1;
        if (currentLocationIndex == 0) {
            processDeletionOfFirstLocation(location, currentLocationIndex);
        } else if (currentLocationIndex != lastLocationIndex) {
            processDeletionOfLocationInCenter(location, currentLocationIndex);
        } else {
            processDeletionOfLastLocation(location);
        }
    }

    private void processDeletionOfFirstLocation(Location location, int currentLocationIndex) {
        Location nextLocation = checkpoints.get(currentLocationIndex + 1);
        List<LatLng> pointsFromCurrentLocation = nextLocation.getPointsFromPreviousLocation();
        List<LatLng> polylinePoints = polyline.getPoints();
        polylinePoints.removeAll(pointsFromCurrentLocation);
        polyline.setPoints(polylinePoints);
        nextLocation.setPointsFromPreviousLocation(new ArrayList<>());
        checkpoints.remove(location);
    }

    private void processDeletionOfLocationInCenter(Location location, int currentLocationIndex) {
        Location previousLocation = checkpoints.get(currentLocationIndex - 1);
        Location nextLocation = checkpoints.get(currentLocationIndex + 1);
        ArrayList<LatLng> newPolylinePoints = new ArrayList<>();
        for (int i = 1; i < currentLocationIndex; i++) {
            newPolylinePoints.addAll(checkpoints.get(i).getPointsFromPreviousLocation());
        }

        ApiResult apiResult = getApiResultForPoints(previousLocation, nextLocation);
        nextLocation.setApiResult(apiResult);
        List<LatLng> pointsBetweenLocations = apiResult.getPoints();
        nextLocation.setPointsFromPreviousLocation(pointsBetweenLocations);
        newPolylinePoints.addAll(pointsBetweenLocations);
        for (int i = currentLocationIndex + 2; i < checkpoints.size(); i++) {
            newPolylinePoints.addAll(checkpoints.get(i).getPointsFromPreviousLocation());
        }
        polyline.setPoints(newPolylinePoints);
        checkpoints.remove(location);
    }

    private void processDeletionOfLastLocation(Location location) {
        List<LatLng> polylinePoints = polyline.getPoints();
        polylinePoints.removeAll(location.getPointsFromPreviousLocation());
        polyline.setPoints(polylinePoints);
        checkpoints.remove(location);
    }

    private ApiResult getApiResultForPoints(Location firstLocation, Location secondLocation) {
        try {
            return bicycleRouteUtil.getPathForLocations(Arrays.asList(firstLocation, secondLocation));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResult();
    }

    public List<Location> getCheckpoints() {
        return checkpoints;
    }

    public void setCheckpoints(List<Location> checkpoints) {
        this.checkpoints = checkpoints;
    }

    public String getName() {
        return name;
    }

    public long getDistance() {
        long distance = 0;
        for (Location location : checkpoints) {
            distance += location.getDistanceInMeters();
        }
        return distance;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }

    public DateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(DateTime startTime) {
        this.startTime = startTime;
    }

    public DateTime getExpectedArrival() {
        return expectedArrival;
    }

    public void setExpectedArrival(DateTime expectedArrival) {
        this.expectedArrival = expectedArrival;
    }

    public boolean isCanBeStarted() {
        return canBeStarted;
    }

    public void setCanBeStarted(boolean canBeStarted) {
        this.canBeStarted = canBeStarted;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public static class Builder {

        private Route route;

        public Builder() {
            this.route = new Route();
        }

        public Builder withApiKey(String apiKey) {
            this.route.bicycleRouteUtil = new BicycleRouteUtil(apiKey);
            return this;
        }

        public Builder withName(String name) {
            this.route.name = name;
            return this;
        }

        public Builder withIsRunning(boolean isRunning) {
            this.route.setRunning(isRunning);
            return this;
        }

        public Builder withStartTime(DateTime startTime) {
            this.route.setStartTime(startTime);
            return this;
        }

        public Builder withExpectedArrivalTime(DateTime expectedArrivalTime) {
            this.route.setExpectedArrival(expectedArrivalTime);
            return this;
        }

        public Builder withAverageSpeed(Double averageSpeed) {
            this.route.setAverageSpeed(averageSpeed);
            return this;
        }

        public Route build() {
            return route;
        }
    }
}