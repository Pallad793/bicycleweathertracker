package com.example.bicycleweathertracker.utils;

import org.joda.time.DateTime;

public class ExpectedArrivalUtil {

    private ExpectedArrivalUtil() {
    }

    public static DateTime countExpectedArrivalDateTime(double averageSpeed, DateTime start, long distanceInMeters) {
        double speedInMetersPerSecond = averageSpeed / 3.6;
        int seconds = (int) (distanceInMeters / speedInMetersPerSecond);
        return start.plusSeconds(seconds);
    }
}
