package com.example.bicycleweathertracker.utils;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateTimeUtil {

    private DateTimeUtil() {
    }

    public static String formatDateTimeToView(DateTime dateTime) {
        if (dateTime == null) {
            return "";
        }
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return formatter.format(parser.parse(dateTime.toLocalDateTime().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
