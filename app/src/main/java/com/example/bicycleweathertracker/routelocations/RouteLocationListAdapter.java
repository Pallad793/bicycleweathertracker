package com.example.bicycleweathertracker.routelocations;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.bicycleweathertracker.Location;
import com.example.bicycleweathertracker.R;
import com.example.bicycleweathertracker.weatherapi.WeatherUtil;
import com.thbs.skycons.library.SkyconView;

import java.util.List;
import java.util.Optional;

import tk.plogitech.darksky.forecast.model.DataPoint;

import static com.example.bicycleweathertracker.utils.DateTimeUtil.formatDateTimeToView;

public class RouteLocationListAdapter extends ArrayAdapter<Location> {

    private final boolean isRunning;

    private static class ViewHolder {
        private TextView locationLongitude;
        private TextView locationLatitude;
        private TextView locationLongitudeLabel;
        private TextView locationLatitudeLabel;
        private TextView locationSequenceNumber;
        private TextView locationTemperature;
        private TextView locationPrecipitation;
        private TextView locationEndTimeLabel;
        private TextView locationEndTime;
        private SkyconView weatherView;
    }

    public RouteLocationListAdapter(@NonNull Context context, int resource, @NonNull List<Location> objects, boolean isRunning) {
        super(context, resource, objects);
        this.isRunning = isRunning;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        RouteLocationListAdapter.ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new RouteLocationListAdapter.ViewHolder();
            convertView = LayoutInflater.from(this.getContext()).inflate(R.layout.one_location_card_view, null);
            viewHolder.locationLatitudeLabel = convertView.findViewById(R.id.locationLatitudeLabel);
            viewHolder.locationLatitude = convertView.findViewById(R.id.locationLatitude);
            viewHolder.locationLongitudeLabel = convertView.findViewById(R.id.locationLongitudeLabel);
            viewHolder.locationLongitude = convertView.findViewById(R.id.locationLongitude);
            viewHolder.locationSequenceNumber = convertView.findViewById(R.id.locationSequenceNumber);
            viewHolder.locationTemperature = convertView.findViewById(R.id.locationTemperature);
            viewHolder.locationPrecipitation = convertView.findViewById(R.id.locationPrecipitation);
            viewHolder.weatherView = convertView.findViewById(R.id.weatherView);
            viewHolder.locationEndTimeLabel = convertView.findViewById(R.id.locationEndTimeLabel);
            viewHolder.locationEndTime = convertView.findViewById(R.id.locationEndTime);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (RouteLocationListAdapter.ViewHolder) convertView.getTag();
        }

        Location location = getItem(position);
        if (location != null) {
            viewHolder.locationSequenceNumber.setText(String.valueOf(location.getSequenceNumber()));

            if (isRunning) {
                viewHolder.locationLatitudeLabel.setVisibility(View.GONE);
                viewHolder.locationLatitude.setVisibility(View.GONE);
                viewHolder.locationLongitudeLabel.setVisibility(View.GONE);
                viewHolder.locationLongitude.setVisibility(View.GONE);
                viewHolder.locationEndTimeLabel.setVisibility(View.VISIBLE);

                String expectedArrival = formatDateTimeToView(location.getExpectedArrival());
                viewHolder.locationEndTime.setText(expectedArrival);

                viewHolder.locationEndTime.setVisibility(View.VISIBLE);
                fillWeatherView(viewHolder, location);
                if (location.getHourlyDataPoint().getTemperature() != null) {
                    viewHolder.locationTemperature.setVisibility(View.VISIBLE);
                    viewHolder.locationTemperature.setText(String.format("%.1f%s", location.getHourlyDataPoint().getTemperature(), "\u00B0C"));
                }
                if (location.getHourlyDataPoint().getPrecipIntensity() != null
                        && location.getHourlyDataPoint().getPrecipIntensity() >= 0.1) {
                    viewHolder.locationPrecipitation.setVisibility(View.VISIBLE);
                    viewHolder.locationPrecipitation.setText(String.format("%.1f%s", location.getHourlyDataPoint().getPrecipIntensity(), "mm"));
                }
            } else {
                viewHolder.locationLatitude.setText(String.valueOf(location.getLatitude()));
                viewHolder.locationLongitude.setText(String.valueOf(location.getLongitude()));
                viewHolder.weatherView.setVisibility(View.GONE);
            }

        }

        return convertView;
    }

    private void fillWeatherView(ViewHolder viewHolder, Location location) {
        RelativeLayout weatherViewParent = (RelativeLayout) viewHolder.weatherView.getParent();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) viewHolder.weatherView.getLayoutParams();
        weatherViewParent.removeView(viewHolder.weatherView);
        String iconNameOrDefault = Optional.ofNullable(location.getHourlyDataPoint())
                .map(DataPoint::getIcon)
                .orElse("");
        viewHolder.weatherView = WeatherUtil.getWeatherView(iconNameOrDefault, getContext());

        viewHolder.weatherView.setLayoutParams(params);
        weatherViewParent.addView(viewHolder.weatherView);
    }
}
