package com.example.bicycleweathertracker;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.bicycleweathertracker.database.converters.RouteHelper;
import com.example.bicycleweathertracker.database.entity.RouteEntity;
import com.example.bicycleweathertracker.database.repository.LocationRepository;
import com.example.bicycleweathertracker.database.repository.RouteRepository;
import com.example.bicycleweathertracker.utils.DateTimeUtil;
import com.example.bicycleweathertracker.weatherapi.WeatherHelper;
import com.example.bicycleweathertracker.weatherapi.WeatherUtil;
import com.thbs.skycons.library.SkyconView;

import org.joda.time.DateTime;

import java.util.List;
import java.util.Optional;

import tk.plogitech.darksky.forecast.model.DataPoint;

public class MainActivity extends NavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewStub stub = findViewById(R.id.layout_stub);

        if (RouteRepository.isAnyRouteRunning(getApplicationContext())) {
            showRouteStartedView(stub);
        } else {
            showNoRouteStartedView(stub);
        }
    }

    private void showRouteStartedView(ViewStub stub) {

        RouteEntity runningRoute = RouteRepository.getRunningRoute(getApplicationContext());
        Route route = RouteHelper.createRoute(this, runningRoute);
        List<Location> allLocationsForRoute = LocationRepository.getAllLocationsForRoute(getApplicationContext(), route.getName());
        route.setCheckpoints(allLocationsForRoute);
        WeatherHelper.fillLocationsWeather(route.getCheckpoints());

        int index = findVisitedCheckpointIndex(route);

        if (index != -1) {
            stub.setLayoutResource(R.layout.dashboard_route_started);
            stub.inflate();

            TextView runningRouteNameTextView = findViewById(R.id.runningRouteNameTextView);
            runningRouteNameTextView.setText(runningRoute.getName());

            fillRouteView(route, index);

            findViewById(R.id.routeDetailsButton).setOnClickListener(click -> {
                Intent routesIntent = new Intent(this, RouteLocationsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("route", route);
                routesIntent.putExtras(bundle);
                startActivity(routesIntent);
            });

        } else {
            route.setRunning(false);
            RouteRepository.update(getApplicationContext(), RouteHelper.createRouteEntity(route));
            stub.setLayoutResource(R.layout.dashboard_route_finished);
            stub.inflate();

            TextView routeFinished = findViewById(R.id.routeFinished);
            routeFinished.setText(runningRoute.getName());

            TextView routeFinishedSummary = findViewById(R.id.routeFinishedSummary);
            long diff = route.getExpectedArrival().getMillis() - route.getStartTime().getMillis();
            Double hours = Double.valueOf(diff / (1000 * 60 * 60));
            Double minutes = Double.valueOf(diff / (1000 * 60) - hours.longValue() * 60);
            routeFinishedSummary.setText(String.format("You have beaten %.3f km in %d h %d min", (double) route.getDistance() / 1000, hours.longValue(), minutes.longValue()));

            findViewById(R.id.chooseRouteButtonAfterFinished).setOnClickListener(click -> {
                Intent routesIntent = new Intent(this, RoutesActivity.class);
                selectedItemIndex = 2;
                startActivity(routesIntent);
            });
        }
    }

    private void fillRouteView(Route route, int index) {
        TextView startTime = findViewById(R.id.startTime);
        startTime.setText(DateTimeUtil.formatDateTimeToView(route.getStartTime()));

        TextView arrivalTime = findViewById(R.id.arrivalTime);
        arrivalTime.setText(DateTimeUtil.formatDateTimeToView(route.getExpectedArrival()));

        TextView averageSpeed = findViewById(R.id.averageSpeed);
        averageSpeed.setText(String.format("%.1f", route.getAverageSpeed()));

        TextView checkpoints = findViewById(R.id.checkpoints);
        checkpoints.setText(String.format("%d / %d", index+1, route.getCheckpoints().size()));

        long achievedDistance = calculateAchievedDistance(route);

        TextView distance = findViewById(R.id.distance);
        distance.setText(String.format("%.3f / %.3f", (double)achievedDistance/1000, (double)route.getDistance()/1000));

        TextView nextCheckpointTextView = findViewById(R.id.nextCheckpointTextView);
        nextCheckpointTextView.setText(String.format("%d", index+2));

        TextView arrivalTimeTextView = findViewById(R.id.arrivalTimeTextView);
        Location nextLocation = route.getCheckpoints().get(index + 1);
        arrivalTimeTextView.setText(DateTimeUtil.formatDateTimeToView(nextLocation.getExpectedArrival()));

        fillWeatherView(nextLocation);

    }

    private long calculateAchievedDistance(Route route) {
        DateTime startTime = route.getStartTime();
        DateTime now = DateTime.now();
        double speedInMetersPerSecond = route.getAverageSpeed() / 3.6;
        double diff = (double)(now.getMillis() - startTime.getMillis()) / 1000;

        return Double.valueOf(speedInMetersPerSecond * diff).longValue();
    }

    private int findVisitedCheckpointIndex(Route route) {
        Optional<Location> first = route.getCheckpoints().stream()
                .filter(location -> location.getExpectedArrival().isAfterNow())
                .findFirst();

        return first.map(location -> location.getSequenceNumber() - 2)
                .orElse(-1);
    }

    private void fillWeatherView(Location location) {
        if (location.getHourlyDataPoint().getTemperature() != null) {
            TextView viewById = findViewById(R.id.nextLocationTemperature);
            viewById.setVisibility(View.VISIBLE);
            viewById.setText(String.format("%.1f%s", location.getHourlyDataPoint().getTemperature(), "\u00B0C"));
        }
        if (location.getHourlyDataPoint().getPrecipIntensity() != null
                && location.getHourlyDataPoint().getPrecipIntensity() >= 0.1) {
            TextView viewById = findViewById(R.id.nextLocationPrecipitation);
            viewById.setVisibility(View.VISIBLE);
            viewById.setText(String.format("%.1f%s", location.getHourlyDataPoint().getPrecipIntensity(), "mm"));
        }

        RelativeLayout weatherViewParent = (RelativeLayout) findViewById(R.id.weatherViewNextCheckpoint).getParent();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) findViewById(R.id.weatherViewNextCheckpoint).getLayoutParams();
        weatherViewParent.removeView(findViewById(R.id.weatherViewNextCheckpoint));
        String iconNameOrDefault = Optional.ofNullable(location.getHourlyDataPoint())
                .map(DataPoint::getIcon)
                .orElse("");
        SkyconView weatherView = WeatherUtil.getWeatherView(iconNameOrDefault, getApplicationContext());

        weatherView.setLayoutParams(params);
        weatherViewParent.addView(weatherView);
    }

    private void showNoRouteStartedView(ViewStub stub) {
        stub.setLayoutResource(R.layout.dashboard_no_route_started);
        stub.inflate();
        Button chooseRouteButton = findViewById(R.id.chooseRouteButton);
        chooseRouteButton.setOnClickListener(click -> {
            Intent routesIntent = new Intent(this, RoutesActivity.class);
            selectedItemIndex = 2;
            startActivity(routesIntent);
        });
    }
}