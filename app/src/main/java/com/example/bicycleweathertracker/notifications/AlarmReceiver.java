package com.example.bicycleweathertracker.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.bicycleweathertracker.MainActivity;

public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        boolean isNewReminder = setReminderAfterReboot(context, intent);

        if (isNewReminder) {
            return;
        }
        NotificationScheduler.showNotification(context, MainActivity.class, intent);
    }

    private boolean setReminderAfterReboot(Context context, Intent intent) {
        if (context != null && intent != null && Intent.ACTION_BOOT_COMPLETED.equalsIgnoreCase(intent.getAction())) {
            SharedData sharedData = new SharedData(context);
            NotificationScheduler.setReminder(context, AlarmReceiver.class, sharedData.getNotificationDate(),
                    sharedData.getNotificationRouteName(), sharedData.getNotificationLocationId());
            return true;
        }
        return false;
    }
}
