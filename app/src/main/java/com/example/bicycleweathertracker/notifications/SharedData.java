package com.example.bicycleweathertracker.notifications;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bicycleweathertracker.database.converters.TimestampConverter;

import org.joda.time.DateTime;

public class SharedData {

    private static final String SHARED_PREFERENCES = "SharedPreferences";

    private final SharedPreferences sharedPreferences;
    private final SharedPreferences.Editor sharedPreferencesEditor;

    public static final String NOTIFICATION_ROUTE_NAME = "notification_route_name";
    public static final String NOTIFICATION_LOCATION_ID = "notification_location_id";
    private static final String NOTIFICATION_DATE = "notification_date";

    public SharedData(Context context) {
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES, Context.MODE_PRIVATE);
        this.sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.apply();
    }

    public String getNotificationRouteName() {
        return sharedPreferences.getString(NOTIFICATION_ROUTE_NAME, null);
    }

    public void setNotificationRouteName(String notificationRouteName) {
        sharedPreferencesEditor.putString(NOTIFICATION_ROUTE_NAME, notificationRouteName);
        sharedPreferencesEditor.commit();
    }

    public Integer getNotificationLocationId() {
        int locationId = sharedPreferences.getInt(NOTIFICATION_LOCATION_ID, -1);
        return locationId != -1 ? locationId : null;
    }

    public void setNotificationLocationId(Integer notificationLocationId) {
        sharedPreferencesEditor.putInt(NOTIFICATION_LOCATION_ID, notificationLocationId);
        sharedPreferencesEditor.commit();
    }

    public DateTime getNotificationDate() {
        String dateTimeString = sharedPreferences.getString(NOTIFICATION_DATE, null);
        return TimestampConverter.fromTimestamp(dateTimeString);
    }

    public void setNotificationDate(DateTime dateTime) {
        sharedPreferencesEditor.putString(NOTIFICATION_DATE, TimestampConverter.fromDate(dateTime));
        sharedPreferencesEditor.commit();
    }


}
