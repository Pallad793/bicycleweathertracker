package com.example.bicycleweathertracker.notifications;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.bicycleweathertracker.Location;
import com.example.bicycleweathertracker.R;
import com.example.bicycleweathertracker.database.repository.LocationRepository;
import com.example.bicycleweathertracker.weatherapi.WeatherHelper;

import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

import tk.plogitech.darksky.forecast.model.HourlyDataPoint;

public class NotificationScheduler {

    private static final int REQUEST_CODE = 1234;
    private static final String CHANNEL_ID = "NotificationChannel";
    private static final String CHANNEL_TITLE = "NotificationChannelTitle";
    private static final String NOTIFICATION_TITLE = "You are close to %s. checkpoint";
    private static final String NOTIFICATION_CONTENT = "Current weather is %s: temperature is %s";
    private static final String OPTIONAL_PRECIPITATION = ", precipitation is %s";
    private static final String COMMON_ERROR = "ERROR";
    private static final String DOUBLE_FORMAT = "%.1f%s";


    public static void setReminder(Context context, Class<?> clazz, DateTime dateTime, String routeName, Integer locationSequenceNumber) {
        Calendar calendar = Optional.ofNullable(dateTime)
                .map(dt -> dt.toCalendar(new Locale("pl")))
                .orElse(Calendar.getInstance());
       // Calendar calendar = Calendar.getInstance();
//        calendar.add(Calendar.MINUTE, 1);

        cancelReminder(context, clazz);

        ComponentName receiver = new ComponentName(context, clazz);
        PackageManager packageManager = context.getPackageManager();

        packageManager.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent = new Intent(context, clazz);
        intent.putExtra(SharedData.NOTIFICATION_ROUTE_NAME, routeName);
        intent.putExtra(SharedData.NOTIFICATION_LOCATION_ID, locationSequenceNumber.intValue());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Optional.ofNullable(alarmManager).ifPresent(am -> am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent));
        Log.i("NotificationScheduler.setReminder", String.format("Notification scheduled on %s", calendar.getTime().toString()));
    }

    public static void cancelReminder(Context context, Class<?> clazz) {
        ComponentName receiver = new ComponentName(context, clazz);
        PackageManager packageManager = context.getPackageManager();

        packageManager.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        Intent intent = new Intent(context, clazz);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Optional.ofNullable(alarmManager).ifPresent(am -> am.cancel(pendingIntent));
        pendingIntent.cancel();
    }

    public static void showNotification(Context context, Class<?> clazz, Intent intent) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent notificationIntent = new Intent(context, clazz);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context)
                .addParentStack(clazz)
                .addNextIntent(notificationIntent);


        Location location = prepareLocation(context, intent);

        if (location == null) {
            return;
        }

        String title = prepareTitle(location);
        String content = prepareContent(location);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(content))
                .setContentText(content)
                .setSound(alarmSound)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .build();


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_TITLE, NotificationManager.IMPORTANCE_HIGH);
        Optional.ofNullable(notificationManager).ifPresent(nm -> nm.createNotificationChannel(channel));
        Optional.ofNullable(notificationManager).ifPresent(nm -> nm.notify(REQUEST_CODE, notification));


        scheduleNextNotification(context, location);
    }

    private static void scheduleNextNotification(Context context, Location previousLocation) {
        int nextSequenceNumber = previousLocation.getSequenceNumber() + 1;
        String routeName = previousLocation.getRouteName();
        Location nextLocation = fetchLocation(context, routeName, nextSequenceNumber);

        if (nextLocation == null) {
            Log.i("NotificationScheduler.scheduleNextNotification",
                    String.format("Could not send notification for routeName=[%s], sequenceNumber=[%s], probably does not exist", routeName, nextSequenceNumber));
            return;
        }
        scheduleNotification(context, routeName, previousLocation, nextLocation);
    }

    private static Location prepareLocation(Context context, Intent intent) {
        String routeName = intent.getStringExtra(SharedData.NOTIFICATION_ROUTE_NAME);
        int sequenceNumber = intent.getIntExtra(SharedData.NOTIFICATION_LOCATION_ID, -1);

        Location location = fetchLocation(context, routeName, sequenceNumber);

        if (location == null) {
            Log.e("NotificationScheduler.showNotification",
                    String.format("Could not send notification for routeName=[%s], sequenceNumber=[%s]", routeName, sequenceNumber));
        } else {
            WeatherHelper.fillLocationsWeather(Collections.singletonList(location));
        }
        return location;
    }

    private static Location fetchLocation(Context context, String routeName, int sequenceNumber) {
        if (routeName == null || sequenceNumber == -1) {
            return null;
        }
        return LocationRepository.getLocationForRouteNameWithSequenceNumber(context, routeName, sequenceNumber);
    }

    private static String prepareTitle(Location location) {
        return String.format(NOTIFICATION_TITLE, location.getSequenceNumber());
    }

    private static String prepareContent(Location location) {
        HourlyDataPoint hourlyDataPoint = location.getHourlyDataPoint();
        if (hourlyDataPoint != null && hourlyDataPoint.getTemperature() != null) {
            String temperature = String.format(DOUBLE_FORMAT, hourlyDataPoint.getTemperature(), "\u00B0C");
            String weather = Optional.ofNullable(hourlyDataPoint.getIcon())
                    .map(s -> s.replaceAll("-", " "))
                    .orElse(COMMON_ERROR);

            Double precipitation = hourlyDataPoint.getPrecipIntensity();
            if (precipitation != null && precipitation >= 0.1) {
                String precipitationString = String.format(DOUBLE_FORMAT, precipitation, "mm");
                return String.format(NOTIFICATION_CONTENT + OPTIONAL_PRECIPITATION, weather, temperature, precipitationString);
            }
            return String.format(NOTIFICATION_CONTENT, weather, temperature);
        }
        return COMMON_ERROR;
    }

    public static void scheduleNotification(Context context, String routeName, Location firstLocation, Location nextLocation) {
        long diff = nextLocation.getExpectedArrival().getMillis() - firstLocation.getExpectedArrival().getMillis();
        DateTime dateTime = nextLocation.getExpectedArrival().minus((long) (diff * 0.2));
        SharedData sharedData = new SharedData(context);
        sharedData.setNotificationDate(dateTime);
        sharedData.setNotificationRouteName(routeName);
        sharedData.setNotificationLocationId(nextLocation.getSequenceNumber());
        NotificationScheduler.setReminder(context, AlarmReceiver.class, dateTime, routeName, nextLocation.getSequenceNumber());
    }

}
