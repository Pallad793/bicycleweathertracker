package com.example.bicycleweathertracker;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.bicycleweathertracker.config.Constants;
import com.example.bicycleweathertracker.database.entity.SystemConfiguration;
import com.example.bicycleweathertracker.database.repository.SystemConfigurationRepository;
import com.example.bicycleweathertracker.settingsmodel.SpeedTextWatcher;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class NavigationActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static int selectedItemIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(selectedItemIndex).setChecked(true);
        prepareDatabase();
    }

    private void prepareDatabase() {
        String averageSpeed = SystemConfigurationRepository.getSystemConfigurationByKey(getApplicationContext(), Constants.AVERAGE_SPEED_KEY);

        if (averageSpeed == null) {
            SystemConfiguration systemConfiguration = new SystemConfiguration.Builder()
                    .withKey(Constants.AVERAGE_SPEED_KEY)
                    .withValue(String.valueOf(Constants.DEFAULT_AVERAGE_SPEED))
                    .build();
            SystemConfigurationRepository.insert(getApplicationContext(), systemConfiguration);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            View dialogView = LayoutInflater.from(this).inflate(R.layout.settings_dialog, null);
            AlertDialog dialog = new AlertDialog.Builder(this).setView(dialogView).create();
            dialog.setCanceledOnTouchOutside(true);

            TextInputEditText speedTextInput = dialogView.findViewById(R.id.averageSpeedInput);
            double averageSpeed = SystemConfigurationRepository.getAverageSpeedOrDefault(getApplicationContext());
            speedTextInput.setText(String.valueOf(averageSpeed));
            speedTextInput.addTextChangedListener(new SpeedTextWatcher(dialogView, speedTextInput));
            dialogView.findViewById(R.id.averageSpeedButton).setOnClickListener(click -> {
                TextInputEditText inputText = dialogView.findViewById(R.id.averageSpeedInput);
                String newAverageSpeed = Objects.toString(inputText.getText());
                SystemConfiguration systemConfiguration = new SystemConfiguration.Builder()
                        .withKey(Constants.AVERAGE_SPEED_KEY)
                        .withValue(newAverageSpeed)
                        .build();
                SystemConfigurationRepository.update(getApplicationContext(), systemConfiguration);
                dialog.cancel();
            });

            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent homeIntent = new Intent(this, MainActivity.class);
            selectedItemIndex = 0;
            startActivity(homeIntent);
        } else if (id == R.id.nav_map) {
            Intent mapIntent = new Intent(this, MapsActivity.class);
            selectedItemIndex = 1;
            startActivity(mapIntent);
        } else if (id == R.id.nav_routes) {
            Intent routesIntent = new Intent(this, RoutesActivity.class);
            selectedItemIndex = 2;
            startActivity(routesIntent);
        }
        MarkerInfoView.resetCheckpointsCounter();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}