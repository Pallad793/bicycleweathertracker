package com.example.bicycleweathertracker;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewStub;

import com.example.bicycleweathertracker.database.converters.LocationHelper;
import com.example.bicycleweathertracker.database.converters.RouteHelper;
import com.example.bicycleweathertracker.database.entity.RouteEntity;
import com.example.bicycleweathertracker.database.repository.LocationRepository;
import com.example.bicycleweathertracker.database.repository.RouteRepository;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.Objects;

public class MapsActivity extends NavigationActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private Route createdRoute;
    private ArrayList<Marker> markers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewStub stub = findViewById(R.id.layout_stub);
        stub.setLayoutResource(R.layout.activity_maps);
        stub.inflate();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        Objects.requireNonNull(mapFragment).getMapAsync(this);

        View dialogView = LayoutInflater.from(this).inflate(R.layout.new_route_name_dialog, null);
        AlertDialog dialog = new AlertDialog.Builder(this).setView(dialogView).create();
        dialog.setCanceledOnTouchOutside(false);

        dialogView.findViewById(R.id.newRouteOkButton).setOnClickListener(getOnNewRouteClickListener(dialogView, dialog));

        dialog.show();
    }

    private View.OnClickListener getOnNewRouteClickListener(View dialogView, AlertDialog dialog) {
        return click -> {
            TextInputEditText inputText = dialogView.findViewById(R.id.newRouteNameInput);
            Polyline polyline = mMap.addPolyline(new PolylineOptions().width(5).color(Color.RED));
            createdRoute = new Route(Objects.toString(inputText.getText()).trim(), polyline, getString(R.string.google_maps_key));
            RouteEntity result = RouteRepository.getRouteByName(getApplicationContext(), createdRoute.getName());
            if (result == null) {
                dialog.cancel();
            } else {
                dialogView.findViewById(R.id.routeNameExistsTextView).setVisibility(View.VISIBLE);
            }
        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // initial zoom on Warsaw
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(52, 20), 8f));
        mMap.getUiSettings().setZoomControlsEnabled(true);

        FloatingActionButton addButton = findViewById(R.id.addToRouteButton);
        FloatingActionButton deleteButton = findViewById(R.id.deleteCheckpointButton);
        FloatingActionButton saveRouteButton = findViewById(R.id.saveRouteButton);

        saveRouteButton.setOnClickListener(click -> saveRoute(click));

        mMap.setOnMapLongClickListener(getOnMapLongClickListener(addButton, deleteButton, saveRouteButton));
        mMap.setOnMapClickListener(latLng -> {
            addButton.hide();
            deleteButton.hide();
            saveRouteButton.show();
        });
    }

    private void saveRoute(View click) {
        if (createdRoute.getCheckpoints().size() >= 2) {
            RouteRepository.insert(getApplicationContext(), RouteHelper.createRouteEntity(createdRoute));
            LocationRepository.insertAll(getApplicationContext(), LocationHelper.createLocationEntities(createdRoute));
            Intent routesIntent = new Intent(this, RoutesActivity.class);
            selectedItemIndex = 2;
            startActivity(routesIntent);
            MarkerInfoView.resetCheckpointsCounter();
        } else {
            Snackbar.make(click, "Route must have at least 2 points!", Snackbar.LENGTH_SHORT).show();
        }
    }

    private GoogleMap.OnMapLongClickListener getOnMapLongClickListener(FloatingActionButton addButton, FloatingActionButton deleteButton, FloatingActionButton saveRouteButton) {
        return latLng -> {
            Marker marker = mMap.addMarker(new MarkerOptions().position(latLng));

            Location location = new Location(latLng, createdRoute.getName());
            marker.setTag(location);

            markers.add(marker);
            MarkerInfoView markerWindowView = new MarkerInfoView(getLayoutInflater(), createdRoute,
                    addButton, deleteButton, saveRouteButton, markers);
            mMap.setInfoWindowAdapter(markerWindowView);
            marker.showInfoWindow();
        };
    }
}