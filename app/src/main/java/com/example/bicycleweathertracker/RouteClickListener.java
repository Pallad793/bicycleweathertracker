package com.example.bicycleweathertracker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.bicycleweathertracker.weatherapi.WeatherHelper;

public class RouteClickListener implements View.OnClickListener {

    private final Context context;

    private final Route route;

    public RouteClickListener(Context context, Route route) {
        this.context = context;
        this.route = route;
    }

    @Override
    public void onClick(View v) {
        Intent routesIntent = new Intent(context, RouteLocationsActivity.class);
        Bundle bundle = new Bundle();
        WeatherHelper.fillLocationsWeather(route.getCheckpoints());
        bundle.putSerializable("route", route);
        routesIntent.putExtras(bundle);
        context.startActivity(routesIntent);
    }
}
