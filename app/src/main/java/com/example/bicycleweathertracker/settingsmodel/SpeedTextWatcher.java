package com.example.bicycleweathertracker.settingsmodel;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import com.example.bicycleweathertracker.R;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class SpeedTextWatcher implements TextWatcher {

    private final View dialogView;

    private final TextInputEditText speedTextInput;

    public SpeedTextWatcher(View dialogView, TextInputEditText speedTextInput) {
        this.dialogView = dialogView;
        this.speedTextInput = speedTextInput;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Button button = dialogView.findViewById(R.id.averageSpeedButton);
        String newStringValue = "0.0";
        String text = Objects.toString(speedTextInput.getText());
        if (!text.isEmpty()) {
            newStringValue = text;
        }
        double newSpeed = Double.parseDouble(newStringValue);
        if (newSpeed > 0.0){
            button.setEnabled(true);
        } else {
            button.setEnabled(false);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
