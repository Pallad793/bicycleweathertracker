package com.example.bicycleweathertracker;

import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.TextView;

import com.example.bicycleweathertracker.allroutes.RouteListAdapter;
import com.example.bicycleweathertracker.database.repository.LocationRepository;
import com.example.bicycleweathertracker.database.repository.RouteRepository;

import java.util.List;
import java.util.Optional;

public class RoutesActivity extends NavigationActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewStub stub = findViewById(R.id.layout_stub);
        stub.setLayoutResource(R.layout.activity_routes);
        stub.inflate();
        ListView allRoutesListView = findViewById(R.id.allRoutesListView);
        TextView noRoutesTextView = findViewById(R.id.noRoutesTextView);

        List<Route> allRoutes = RouteRepository.getAllRoutes(getApplicationContext());
        fillAllRoutesWithLocations(allRoutes);
        fillCanBeStartedIfRequired(allRoutes);
        if (!allRoutes.isEmpty()) {
            noRoutesTextView.setVisibility(View.GONE);

        }

        RouteListAdapter routeListAdapter = new RouteListAdapter(this, R.layout.one_route_card_view, allRoutes);
        allRoutesListView.setAdapter(routeListAdapter);


    }

    private void fillCanBeStartedIfRequired(List<Route> allRoutes) {
        Optional<Route> first = allRoutes.stream().filter(Route::isRunning).findFirst();
        fillCanBeStarted(first, allRoutes);
    }

    private void fillCanBeStarted(Optional<Route> route, List<Route> allRoutes) {
        boolean canBeStarted = !route.isPresent();
        allRoutes.forEach(route1 -> route1.setCanBeStarted(canBeStarted));
        route.ifPresent(route1 -> route1.setCanBeStarted(true));
    }

    private void fillAllRoutesWithLocations(List<Route> allRoutes) {
        allRoutes.forEach(this::fillRouteLocations);
    }

    private void fillRouteLocations(Route route) {
        List<Location> allLocationsForRoute = LocationRepository.getAllLocationsForRoute(getApplicationContext(), route.getName());
        route.setCheckpoints(allLocationsForRoute);
    }
}
